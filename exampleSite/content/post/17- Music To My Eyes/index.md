+++
author = "Oguzhan Boz"
title = "Music To My Eyes"
date = "2021-12-19"
description = "Week 9 / Assignment 7"
categories = [
    "CRP-101",
    "Assignment"
]
tags = [
    "Music",
    "Rhythm",
    "Note",
    "Lines",
    "Repetition",
    "Continutiy",
    "Symmetry",
    "Ups and Downs",
    "Gestalt Principles",
    "Figure-Ground Relationship",
    "Piece and Whole Relationship",
    "Hierarchy and Balance",
    "Solid and Void Relationship",
    "2D and 3D Relationship",
    "Volumetric Relations"

]
image = "IMG-1630-min.jpg"
+++

In the last assignment, I have learned how to make a composition from a piece of music. We have discussed these main concepts:

• Rythm

• Gestalt Principles

• Figure Ground Relationship

• Piece and Whole Relationship

• Hierarchy and Balance

• Solid and Void Relationship

• 2D and 3D Relationship

• Volumetric Relations

I utilized the song “Giant Steps” by John Coltrane.

![](IMG-1630-min.jpg)
