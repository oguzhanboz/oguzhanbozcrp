+++
author = "Oguzhan Boz"
title = "In-class-exercise: Site Analysis-point, line, surface"
date = "2022-06-26"
description = "Week 6"
categories = [
    "CRP-102",
    "In-class Exercise"
]
tags = [
    "Mapping",
    "Analysis",
    "Public Spaces",
    "Human Movement",
    "Inner and Outer Relations"
]
image = "pls.jpg"
+++

![](pls.jpg)
