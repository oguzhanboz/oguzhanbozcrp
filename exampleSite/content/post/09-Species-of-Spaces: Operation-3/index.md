+++
author = "Oguzhan Boz"
title = "Species of Spaces: Operation 3"
date = "2021-11-14"
description = "Week 4 / Assignment 3"
categories = [
    "CRP-101",
    "Assignment"
]
tags = [
    "Georges Perec",
    "Species of Spaces",
    "Volumetric Study",
    "Cut",
    "Fold",
    "Bend",
    "Volumes",
    "Interrelations of Forms",
    "Composition Forms",
    "Frame of Reference",
    "Figure-Ground Relationship",
    "Continuity",
    "Repetition and Symmetry",
    "3 Dimensions"
]
image = "OPE3.jpg"
+++

In the last operation, We have learned how to make a volumetric study following the Cut & Fold & Bend exercise. We have discussed these main concepts:

**-** Frame of reference
**-** Interrelations of forms
**-** Composition forms
**-** Repetition and Symmetry

**Repeat the objects to create a pattern, lead to continuity, or provide symmetry.**

![](OPE3.jpg)

I have inspired from this sentence in Georges Perec, Species of Spaces and Other Pieces :

_“I would like there to exist places that are stable, unmoving, intangible, untouched and almost untouchable, unchanging, deep-rooted; places that might be points of reference, of departure, of origin.”_ 

So, I have tried to create a point of reference at the top right corner.

Composition Images from different angles.
(Radial Form)

![](1.jpg)
![](2.jpg)

My first and last project:

![](1.jpg)
![](es.jpg)

In my first project, there were some main mistakes, such as the sizes of the forms and the interrelations of the forms. I couldn’t use too many shapes due to the dimensions of other figures, and I have tried to make a composition with pairs to use proximity, but I have failed. I have forgotten to look at the whole piece. When I have revised it, I have changed the sizes of the forms and tried to focus on the entire composition. Also, I wanted to make my design more effective by using some interrelations of forms and ensuring continuity.
