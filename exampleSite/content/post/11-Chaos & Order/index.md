+++
author = "Oguzhan Boz"
title = "Chaos & Order"
date = "2021-11-28"
description = "Week 6 / Assignment 4"
categories = [
    "CRP-101",
    "Assignment"
]
tags = [
    "Chaos",
    "Order",
    "Gestalt Theory",
    "Basic Design Principles",
    "Visual Processing",
    "Interrelations of Forms",
    "Figure-Ground Relationship",
    "Meaningful Composition",
    "Local Symmetry"
]
image = "new.jpg"
+++

_“The whole is other than the sum of the parts.”_ **Kurt Koffka**

In this assignment, We have learned how to make a meaningful composition by utilizing the gestalt principles.

![](new.jpg)

**What we are looking for is not good or bad composition, but meaningful 
composition.**

The main concepts that we discussed in the lectures were:

• Gestalt Theory

• Basic Design Principles

• Visual Processing

• Interrelations of Forms

• Figure and Ground Relationship

This sentence in Baykan GÜNAY, GESTALT THEORY AND CITY PLANNING EDUCATION has become a reference for me:

_“In what Koffka called Gestalt-qualität or ‘form-quality’, ‘when one hears a melody, one 
hears the notes plus something in addition to them which binds them together into a 
tune’ (Koffka, 2000, 1); therefore, you perceive the melody as a whole.”_

If we compare my first and last composition;

![](es.jpg)
![](new.jpg)

*Avoid creating Central Symmetry; use Local Symmetry to create diversity.

In the first project, I have mostly tried using the principles of enclosure 
and continuity. Still, the problem here is that I didn't pay much attention 
to the figure-ground relationship, so the composition looks full of figures 
and has some meaningless shapes. Therefore, I have revised my mistakes 
and used local symmetry, which I did not use in my first composition, in 
the upper right and lower left corners, in my final project.
