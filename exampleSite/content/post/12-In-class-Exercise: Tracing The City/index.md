+++
author = "Oguzhan Boz"
title = "In-class Exercise: Tracing The City"
date = "2021-11-30"
description = "Week 7"
categories = [
    "CRP-101",
    "In-class Exercise",
    "Sketching"
]
tags = [
    "Tracing",
    "Site Sketching",
    "Drawing",
    "Gestalt Principles",
    "Form Relations",
    "Venice City"
]
image = "ttc.jpg"
+++

In this exercise, we traced some parts of Venice City. We utilized Gestalt Principles and draw some shapes to present the principles.

![](ttc.jpg)

I indicated some enclosures, repetitons, continuity, etc. and related them.
