+++
author = "Oguzhan Boz"
title = "In-class Exercise: LetterForm"
date = "2021-10-27"
description = "Week 2"
categories = [
    "CRP-101",
    "In-class Exercise"
]
tags = [
    "Compositional Values",
    "Weight",
    "Size",
    "Color",
    "Position",
    "Rotation"
]
image = "LetterForm.jpg"
+++

In this "Letter Form" exercise, we made a composition by using the first letter of our names. I put different sizes of "O" letter on the ground, and I changed their color. I tried to rotate them, but eventually, nothing changed because of the shape of the letter and the font that I used :)


![](LetterForm.jpg)
