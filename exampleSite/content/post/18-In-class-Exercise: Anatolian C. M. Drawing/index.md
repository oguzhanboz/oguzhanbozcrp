+++
author = "Oguzhan Boz"
title = "In-class Exercise: Anatolian C. M. Drawing"
date = "2021-12-28"
description = "Week 11"
categories = [
    "CRP-101",
    "In-class Exercise",
    "Sketching"
]
tags = [
    "Sketching",
    "Drawing",
    "Pattern",
    "Shapes",
    "Civilizations"
]
image = "acmd.jpg"
+++

In-class Exercise: Anatolian C. M. Drawing content will be uploaded here.

![](acmd.jpg)