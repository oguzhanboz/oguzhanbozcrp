+++
author = "Oguzhan Boz"
title = "From Lines To Volumes"
date = "2021-12-12"
description = "Week 8 / Assignment 6"
categories = [
    "CRP-101",
    "Assignment"
]
tags = [
    "Order",
    "Gestalt",
    "Hierarchy and Balance",
    "Solid and Void Relationship",
    "2D and 3D Relationship",
    "Distribution of the Open and Closed Spaces",
    "Inner Movement",
    "Inner Relations",
    "Volumetric Relations",
    "Diversity",
    "Robustness"
]
image = "Untitled-1.jpg"
+++

In this assignment, I have learned how to create a volumetric study by using the visual composition in the Order Assignment and considering these main concepts:

• Order

• Gestalt

• Hierarchy and Balance

• Solid and Void Relationship

• 2D and 3D Relationship

• Distribution of the Open and Closed Spaces

• Inner Movement

• Volumetric Relations

• Diversity

![](Untitled-1.jpg)
