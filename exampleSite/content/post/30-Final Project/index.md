+++
author = "Oguzhan Boz"
title = "Final Project"
date = "2022-07-02"
description = "Week 14 / Final Project"
categories = [
    "CRP-102",
    "Assignment"
]
tags = [
    "Mapping",
    "Analysis",
    "Public Spaces",
    "Human Movement",
    "Inner and Outer Relations"
]
image = "f5.jpg"
+++

![](f5.jpg)
