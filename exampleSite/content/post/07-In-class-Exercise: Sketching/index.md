+++
author = "Oguzhan Boz"
title = "In-class Exercise: Sketching"
date = "2021-11-08"
description = "Week 4"
categories = [
    "CRP-101",
    "In-class Exercise",
    "Sketching"
]
tags = [
    "Sketching",
    "Drawing",
    "Free Hand"
]
image = "sktch.jpg"
+++

In this exercise, I tried to sketch the visible part of the library when we looked from the front of the faculty of architecture.

![](sktch.jpg)
