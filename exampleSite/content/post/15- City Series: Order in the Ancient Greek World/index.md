+++
author = "Oguzhan Boz"
title = "City Series: Order in the Ancient Greek World"
date = "2021-12-09"
description = "Week 8"
categories = [
    "CRP-101",
    "City Series"
]
tags = [
    "Order",
    "Ancient",
    "Greek",
    "Beauty",
    "Cosmos",
    "Unity and Divisions",
    "Polis",
    "Symmetria",
    "Isonomia",
    "Rythmos"
]
image = "cs.jpg"
+++

![](cs.jpg)