+++
author = "Oguzhan Boz"
title = "FINAL PROJECT 'IN & BETWEEN'"
date = "2022-01-31"
description = "Week 14 / Final Project"
categories = [
    "CRP-101",
    "Assignment"
]
tags = [
    "Analysis",
    "Conceptual Sketch",
    "3D Model",
    "Public Spaces",
    "Human Movement",
    "Inner and Outer Relations",
    "Human Figures",
    "A Unique Story",
    "Reference Point",
    "Solid and Void Relations",
    "Elements of Interests"
]
image = "final.jpg"
+++

![](final.jpg)

For the final project, we analyzed the Faculty of Archtitecture. I started with human movement and indicated the pedesterian alley and some of the paths that people use. Then, I specified social areas, enclosures, and trees. I noticed that there is a relation between them. It is like there is a repetition and they continue. This became reference for me. In addition, I indicated some elements that attract people's attention such as water, shelter, or a big tree. For the conceptual sketch, I created a welcoming area, and drew some paths. I connected pedesterian alley to other paths, and created a public space that has a relation between the other areas.
