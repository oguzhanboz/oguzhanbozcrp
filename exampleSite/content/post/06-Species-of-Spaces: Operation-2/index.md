+++
author = "Oguzhan Boz"
title = "Species of Spaces: Operation 2"
date = "2021-11-07"
description = "Week 3 / Assignment 2"
categories = [
    "CRP-101",
    "Assignment"
]
tags = [
    "Georges Perec",
    "Species of Spaces",
    "Capturing The Essence",
    "Abstraction",
    "Image",
    "Lines",
    "Volumes",
    "Interrelations of Forms",
    "Compositional Values",
    "Frame of Reference",
    "Figure-Ground Relationship",
    "Continuity",
    "Space",
    "Scale"
]
image = "OPE2.jpg"
+++

In the second operation of the Species of Spaces, we have learned how to create a composition with an image, lines, and volumes in a restricted frame following the Cut & Fold & Bend exercise and make a relationship between them. The main concepts that we discussed in the lectures were:

**-** Capturing the essence

**-** Frame of reference

**-** Figure-ground relationship

**-** Continuity

**-** Interrelations of forms

![](OPE2.jpg)

**What makes the figure is the ground**

I have utilized Georges Perec, Species of Spaces and Other Pieces to look for the meaning of the volumes in spatial terms.

_“When nothing arrests our gaze, it carries a very long way. But if it meets with nothing, it sees nothing, it sees only what it meets. Space is what arrests our gaze, what our  sight stumbles over: the obstacle, bricks, an angle, a vanishing point. Space is when it makes an angle, when it stops, when we have to turn for it to start off again. There's nothing ectoplasmic about space; it has edges, it doesn’t go off in all directions, it does all that needs to be done for railway lines to meet well short of infinity.”_ 


I put some images from different angles below.

![](1.jpg)
![](2.jpg)
![](3.jpg)
![](4.jpg)

The images of my first and last project:

![](esk.jpg)
![](OPE2.jpg)

My first project was again like a drawing. The Cut & Fold & Bend part also wasn’t good enough in terms of relationship with the frame, figure-ground, interrelations of forms. That’s why I have revised it according to the discussions and suggestions.
