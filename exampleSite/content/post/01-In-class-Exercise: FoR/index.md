+++
author = "Oguzhan Boz"
title = "In-class Exercise: FoR"
date = "2021-10-21"
description = "Week 1"
categories = [
    "CRP-101",
    "In-class Exercise"
]
tags = [
    "Frame of Reference",
    "Dynamic and Static Composition",
    "Positive and Negative Spaces",
    "Scale",
    "Color",
    "Position"
]
image = "FoR.jpg"
+++

This exercise aims to learn the interrelations between the form and the frame(Frame of Reference).

_"The thing is something that is not itself a thing"_

![](sfor1.jpg)
![](sfor2.jpg)
![](sfor3.jpg)
![](sfor4.jpg)

Here you have many options to put the form somewhere. You can put it at the middle, bottom, top, or corners, but when you put it there, you should ask yourself a question:

**WHY DID I PUT THIS FORM THERE?**

The answers may change, of course. You can say that "I put it there because I wanted it that way!" or "It looked good to my eyes!" but that's not the answer we're looking for. You should find an answer that includes the visual terms. **Dynamic, Static, Dominant...** You should say that "When I put this form at the middle, it has a balance. It has the same distance for all edges, so there is a static composition." When you put it somewhere else, you can say that now it has dynamism. The distances are different. While the two edges try to pull the form to themselves, the other edges try to resist it. What a scene!

![](sdfor3.jpg)
![](sdfor4.jpg)

 Also, we can make the figure more dominant by changing its size and its color.
![](for1.jpg)
![](for2.jpg)
![](for3.jpg)
![](for4.jpg)

![](Forr.jpg)
