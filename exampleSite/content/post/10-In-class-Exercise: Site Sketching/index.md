+++
author = "Oguzhan Boz"
title = "In-class Exercise: Site Sketching"
date = "2021-11-22"
description = "Week 6"
categories = [
    "CRP-101",
    "In-class Exercise",
    "Sketching"
]
tags = [
    "Site Sketching",
    "Drawing",
    "Free Hand"
]
image = "Ss.jpg"
+++

This exercise was the same as the previous sketching exercise, but I sketched another area this time.

![](Ss.jpg)
