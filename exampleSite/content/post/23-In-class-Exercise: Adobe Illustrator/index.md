+++
author = "Oguzhan Boz"
title = "In-class-Exercise: Adobe Illustrator"
date = "2022-06-25"
description = "Week 4"
categories = [
    "CRP-102",
    "In-class Exercise"
]
tags = [
    "Mapping",
    "Analysis",
    "Public Spaces",
    "Human Movement",
    "Inner and Outer Relations"
]
image = "aie.jpg"
+++

![](aie.jpg)
