+++
author = "Oguzhan Boz"
title = "In-class Exercise: Freehand Drawing(Doodling)"
date = "2021-10-26"
description = "Week 2"
categories = [
    "CRP-101",
    "In-class Exercise"
]
tags = [
    "Doodling",
    "Free Hand",
    "Drawing",
    "Lines"
]
image = "Doodling.jpg"
+++

In this exercise, we did some freehand drawings and improved our hand skills. We discovered different line shapes, weights, etc., and made some relations between them.


![](Doodling.jpg)
