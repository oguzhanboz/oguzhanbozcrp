+++
author = "Oguzhan Boz"
title = "In-class-Exercise: Collage Study"
date = "2022-06-24"
description = "Week 4"
categories = [
    "CRP-102",
    "In-class Exercise"
]
tags = [
    "Mapping",
    "Analysis",
    "Public Spaces",
    "Human Movement",
    "Inner and Outer Relations"
]
image = "cs.jpg"
+++

![](cs.jpg)
