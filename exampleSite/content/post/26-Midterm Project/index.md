+++
author = "Oguzhan Boz"
title = "Midterm Project"
date = "2022-06-28"
description = "Week 8 / Midterm Project"
categories = [
    "CRP-102",
    "Assignment"
]
tags = [
    "Mapping",
    "Analysis",
    "Public Spaces",
    "Human Movement",
    "Inner and Outer Relations"
]
image = "mt1.jpg"
+++

![](mt1.jpg)
