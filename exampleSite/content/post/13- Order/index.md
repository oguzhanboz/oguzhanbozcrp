+++
author = "Oguzhan Boz"
title = "Order"
date = "2021-12-05"
description = "Week 7 / Assignment 5"
categories = [
    "CRP-101",
    "Assignment"
]
tags = [
    "Order",
    "Gestalt",
    "Levels of Order",
    "Grammar",
    "Syntax",
    "Semantics",
    "Power Lines",
    "Unity Within Variety Principle",
    "Design in 2D and 3D",
    "Interrelations of Forms"
]
image = "new.jpg"
+++

In this assignment, We have learned what order is and how to make a composition within an order. The main concepts that we discussed in the lectures were:

• Levels of order(Grammar, Syntax, Semantics)

• Power Lines

• Unity Within Variety Principle

• Design in 2D and 3D

![](new.jpg)

I have used this sentence from Rudolf Arnheim, An Essay On Disorder And Order, as a reference:

_“Order makes it possible to focus on what is alike and what is different, what belongs 
together and what is segregated.”_

My first and last composition:

![](new.jpg)
![](es.jpg)

**Let the eye complete!**

In the first project, there were too many small forms, and the relationship between the tiny and big forms reduced the quality of the composition. I used the same patterns but slightly larger ones in the second project, and I avoided creating small forms.

These are the patterns that I used for both projects.

![](1.jpg)
![](2.jpg)
![](3.jpg)
![](es.jpg)

![](11.jpg)
![](12.jpg)
![](13.jpg)
![](new.jpg)

I wanted to answer some questions like,

**WHAT IS ORDER?**

1

**WHY IS ORDER SO IMPORTANT?**

2

**HOW TO UTILIZE ORDER?**

3

**IS ORDER A TOOL OR A RULE?**

4
