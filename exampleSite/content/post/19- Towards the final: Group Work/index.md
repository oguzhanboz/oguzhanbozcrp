+++
author = "Oguzhan Boz"
title = "Towards the final: Group Work"
date = "2022-01-02"
description = "Week 11 / Assignment"
categories = [
    "CRP-101",
    "Assignment"
]
tags = [
    "Mapping",
    "Analysis",
    "Public Spaces",
    "Human Movement",
    "Inner and Outer Relations",
]
image = "ttf.jpg"
+++

![](tff.jpg)
