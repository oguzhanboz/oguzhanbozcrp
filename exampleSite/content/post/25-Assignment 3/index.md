+++
author = "Oguzhan Boz"
title = "Assignment 3-Getting to Know the Site"
date = "2022-06-27"
description = "Week 6 / Assignment"
categories = [
    "CRP-102",
    "Assignment"
]
tags = [
    "Mapping",
    "Analysis",
    "Public Spaces",
    "Human Movement",
    "Inner and Outer Relations"
]
image = "a32.jpg"
+++

![](a32.jpg)
