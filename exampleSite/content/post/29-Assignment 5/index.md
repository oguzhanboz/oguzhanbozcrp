+++
author = "Oguzhan Boz"
title = "Assignment 5"
date = "2022-07-01"
description = "Week 13 / Assignment"
categories = [
    "CRP-102",
    "Assignment"
]
tags = [
    "Mapping",
    "Analysis",
    "Public Spaces",
    "Human Movement",
    "Inner and Outer Relations"
]
image = "a51.jpg"
+++

![](a51.jpg)
