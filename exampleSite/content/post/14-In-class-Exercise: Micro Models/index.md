+++
author = "Oguzhan Boz"
title = "In-class Exercise: Micro Models"
date = "2021-12-08"
description = "Week 8"
categories = [
    "CRP-101",
    "In-class Exercise"
]
tags = [
    "Micro",
    "3D Model",
    "Solid and Void Relationship",
    "2D and 3D Relationship",
    "Open and Closed Spaces",
    "Inner Movement",
    "Inner Relationship"
]
image = "mm.jpg"
+++

In-class Exercise: Micro Models content will be uploaded here.

![](mm.jpg)
