+++
author = "Oguzhan Boz"
title = "Species of Spaces: Operation 1"
date = "2021-10-31"
description = "Week 2 / Assignment 1"
categories = [
    "CRP-101",
    "Assignment"
]
tags = [
    "Georges Perec",
    "Species of Spaces",
    "Capturing The Essence",
    "Abstraction",
    "Compositional Values",
    "Frame of Reference",
    "Figure-Ground Relationship",
    "Continuity",
    "Lines",
    "Space",
    "Scale"
]
image = "OPE1.jpg"
+++

_“A line is a dot that went for a walk.”_ **Paul Klee**

![](OPE1.jpg)

In our first assignment, We have learned the basics of the abstraction and, how to abstract a scene or an object by using lines. We have tried to understand how details change by changing the scale. I wrote some of the main terms about this operation below.

**-** Capturing the essence

**-** Compositional values

**-** Frame of reference

**-** Figure-ground relationship

**-** Continuity

A book by Georges Perec was our reference.

_Species of Spaces and Other Pieces_

In this book, there were lots of sentences to reference, but I chose these two sentences as a reference:

_“Before, there was nothing, or almost nothing; afterwards, there isn’t much, a few signs, but which are enough for there to be a top and a bottom, a beginning and an end, a right and a left, a recto and verso.”_

_“This is how space begins, with words only, signs traced on the blank page.”_

![](soso.jpg)
![](OPE1.jpg)

The image on the left side was my first trial. It was like a drawing rather than abstraction, and the relationship between figure-ground and lines was weak. It was really hard to understand what to do precisely at that time, but after a while, we got familiar. I understood my mistakes and found a solution according to them. In my final trial, my process of capturing the essence was better than the first one. It wasn't like a drawing anymore, even though the line quality is poor!

