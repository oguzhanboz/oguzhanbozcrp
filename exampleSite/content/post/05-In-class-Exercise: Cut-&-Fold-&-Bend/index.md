+++
author = "Oguzhan Boz"
title = "In-class Exercise: Cut & Fold & Bend"
date = "2021-11-02"
description = "Week 3"
categories = [
    "CRP-101",
    "In-class Exercise"
]
tags = [
    "Cut",
    "Fold",
    "Bend",
    "Relationship in 3 Dimensions"
]
image = "cfb.jpg"
+++

The aim of the "Cut & Fold & Bend Exercise" was to make relations in 3 Dimensions. We CUT some papers, FOLD them and created some forms by BENDING them. We brought those forms together and created a meaningul composition.

![](cfb.jpg)
