+++
author = "Oguzhan Boz"
title = "Assignment 4"
date = "2022-06-30"
description = "Week 10 / Assignment"
categories = [
    "CRP-102",
    "Assignment"
]
tags = [
    "Mapping",
    "Analysis",
    "Public Spaces",
    "Human Movement",
    "Inner and Outer Relations"
]
image = "a43.jpg"
+++

![](a43.jpg)
