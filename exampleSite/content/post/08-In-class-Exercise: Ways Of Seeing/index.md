+++
author = "Oguzhan Boz"
title = "In-class Exercise: Ways Of Seeing"
date = "2021-11-09"
description = "Week 4"
categories = [
    "CRP-101",
    "In-class Exercise"
]
tags = [
    "Ways Of Seeing",
    "John Berger",
    "Paintings",
    "Image",
    "Manipulation",
    "Original Meaning",
    "Movement",
    "Sound",
    "Figure-Ground Relationship",
    "Visual Perception",
    "Invention of the Camera",
    "Music & Rhythm"
]
image = "wos.jpg"
+++

WAYS OF SEEING **JOHN BERGER**

![](wos.jpg)

We watched a 30-minute video shot by John Berger. He was talking about the meaning of an image or painting. I took this notes:

Because paintings are silent, stilli and their meaning is no longer attached to them, we can manipulate them easily.
They can be used to make arguments which may different from their original meaning.
Because paintings are **silent and still**, the most obvious way to manipulate them is by using **MOVEMENT and SOUND**.

So;

if we focus just one part of the painting,

if we look closer that part,

if we look in detail

the meaning of that image changes. A figure becomes another thing.


The meaning of an image can be changed according what you see beside it or what comes after it. The impact of an image can be modified.
