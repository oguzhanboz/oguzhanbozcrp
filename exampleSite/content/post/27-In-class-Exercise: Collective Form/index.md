+++
author = "Oguzhan Boz"
title = "In-class-Exercise: Collective Form"
date = "2022-06-29"
description = "Week 9"
categories = [
    "CRP-102",
    "In-class Exercise"
]
tags = [
    "Mapping",
    "Analysis",
    "Public Spaces",
    "Human Movement",
    "Inner and Outer Relations"
]
image = "cf3.jpg"
+++

![](cf3.jpg)
