---
title: About Me
description: ODTÜ/METU-City and Regional Planning
date: '2021-11-01'
aliases:
  - about-us
  - about-hugo
  - contact

menu:
    main: 
        weight: -90
        params:
            icon: user
---

Hi! My name is Oğuzhan Boz. I am studying at Middle East Technical University as a first-year student, and my department is City and Regional Planning. I will use this blog to share my basic design projects and some research related to city and regional planning.

You can contact me via this e-mail: me@oguzhanbozcrp.studio
